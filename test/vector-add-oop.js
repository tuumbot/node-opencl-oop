#!/usr/bin/env node
/** @file vector-add.js
 *
 *
 */
'use strict';

const fs = require('fs');
const path = require('path');

const logger = {
  log: console.log,
};

const BUFFER_SIZE=10;

function VectorAdd() {

  const {
    contextProvider,
    programFactory,
    bufferFactory,
    kernelFactory,
    commandQueueProvider,

    cl,
    OpenCL,
  } = require('../src');

  /* initialize memory */
  let A = new Uint32Array(BUFFER_SIZE);
  let B = new Uint32Array(BUFFER_SIZE);
  let C = new Uint32Array(BUFFER_SIZE);

  for (var i = 0; i < BUFFER_SIZE; i++) {
    A[i] = i;
    B[i] = i * 2;
    C[i] = 10;
  }

  const size = BUFFER_SIZE * Uint32Array.BYTES_PER_ELEMENT; // size in bytes

  const kName = 'vadd'; /* source kernel function name */
  const source = fs.readFileSync(path.join(__dirname, './vector-add.cl')).toString();

  const ocl = new OpenCL({
    logger,
  });

  ocl.createKernel({
    kName: kName,
    source,
    buffers: [
      {
        bName: 'A',
        flags: cl.MEM_READ_ONLY | cl.MEM_USE_HOST_PTR,
        bufferSize: size,
        bufferData: A,
        bufferTypeStr: 'uint*',
      },
      {
        bName: 'B',
        flags: cl.MEM_READ_ONLY | cl.MEM_USE_HOST_PTR,
        bufferSize: size,
        bufferData: B,
        bufferTypeStr: 'uint*',
      },
      {
        bName: 'C',
        flags: cl.MEM_READ_ONLY | cl.MEM_USE_HOST_PTR,
        bufferSize: size,
        bufferData: C,
        bufferTypeStr: 'uint*',
      },
      {
        bName: 'bufferSize',
        flags: null,
        bufferSize: null,
        bufferData: BUFFER_SIZE,
        bufferTypeStr: 'uint',
      },
    ],
  });

  // Execute the OpenCL kernel on the list
  // var localWS = [5]; // process one list at a time
  // var globalWS = [clu.roundUp(localWS, BUFFER_SIZE)]; // process entire list
  var localWS = null;
  var globalWS = [BUFFER_SIZE];

  // Execute (enqueue) kernel
  ocl.enqueue(kName, {
    localWS,
    globalWS
  });

  C = ocl.syncBuffer('C');

  ocl.finish();
  ocl.cleanup();

  printResults(A,B,C);

}

function printResults(A,B,C) {
  //Print input vectors and result vector
  var output = "\nA = ";
  for (var i = 0; i < BUFFER_SIZE; i++) {
    output += A[i] + ", ";
  }
  output += "\nB = ";
  for (var i = 0; i < BUFFER_SIZE; i++) {
    output += B[i] + ", ";
  }
  output += "\nC = ";
  for (var i = 0; i < BUFFER_SIZE; i++) {
    output += C[i] + ", ";
  }

  logger.log(output);
}

VectorAdd();

console.log("\n== Main thread terminated ==");
