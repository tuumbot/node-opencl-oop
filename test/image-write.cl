
__kernel void vImageInvert(__global uchar *input, __global uchar *output, uint iNumElements)
{
  size_t i =  get_global_id(0);

  if(i > iNumElements) return;

  uint o = i * 3;

  output[o] = input[o];
  output[o + 1] = input[o + 1];
  output[o + 2] = 255; // input[o + 2];
}
