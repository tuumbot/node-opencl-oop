#!/usr/bin/env node
/** @file vector-add.js
 *
 *
 */
'use strict';

const fs = require('fs');
const path = require('path');

const logger = {
  log: console.log,
};

const BUFFER_SIZE=10;

function VectorAdd() {

  const {
    contextProvider,
    programFactory,
    bufferFactory,
    kernelFactory,
    commandQueueProvider,

    cl,
  } = require('../src');

  /* initialize memory */
  let A = new Uint32Array(BUFFER_SIZE);
  let B = new Uint32Array(BUFFER_SIZE);
  let C = new Uint32Array(BUFFER_SIZE);

  for (var i = 0; i < BUFFER_SIZE; i++) {
    A[i] = i;
    B[i] = i * 2;
    C[i] = 10;
  }

  const [device, context] = contextProvider();

  logger.log('using device: '+cl.getDeviceInfo(device, cl.DEVICE_VENDOR).trim()+
  ' '+cl.getDeviceInfo(device, cl.DEVICE_NAME));

  const size = BUFFER_SIZE * Uint32Array.BYTES_PER_ELEMENT; // size in bytes

  const aBuffer = bufferFactory({
    context,
    flags: cl.MEM_READ_ONLY | cl.MEM_USE_HOST_PTR,
    bufferSize: size,
    bufferData: A
  });

  const bBuffer = bufferFactory({
    context,
    flags: cl.MEM_READ_ONLY | cl.MEM_USE_HOST_PTR,
    bufferSize: size,
    bufferData: B
  });

  const cBuffer = bufferFactory({
    context,
    flags: cl.MEM_READ_ONLY | cl.MEM_USE_HOST_PTR,
    bufferSize: size,
    bufferData: C
  });

  /* load kernel source code */
  const source = fs.readFileSync(path.join(__dirname, './vector-add.cl')).toString();

  /* compile kernel program */
  const program = programFactory({ context, source });

  /* create kernel structure */
  const kernel = kernelFactory({ device, program, kName: 'vadd'});

  if(!kernel)
  {
    throw new Error(JSON.stringify({kernel}));
  }

  cl.setKernelArg(kernel, 0, "uint*", aBuffer);
  cl.setKernelArg(kernel, 1, "uint*", bBuffer);
  cl.setKernelArg(kernel, 2, "uint*", cBuffer);
  cl.setKernelArg(kernel, 3, "uint", BUFFER_SIZE);

  // Create command queue
  const queue = commandQueueProvider({ context, device });

  if(!queue)
  {
    throw new Error(JSON.stringify({queue}));
  }

  // Execute the OpenCL kernel on the list
  // var localWS = [5]; // process one list at a time
  // var globalWS = [clu.roundUp(localWS, BUFFER_SIZE)]; // process entire list
  var localWS = null;
  var globalWS = [BUFFER_SIZE];

  logger.log("Global work item size: " + globalWS);
  logger.log("Local work item size: " + localWS);

  // Execute (enqueue) kernel
  let t0, t1;

  t0 = new Date();

  cl.enqueueNDRangeKernel(queue, kernel, 1,
      null,
      globalWS,
      localWS);

  t1 = new Date();

  logger.log(`:enqueueNDRangeKernel:`, { deltaTime_ms: (t1 - t0) });


  logger.log("using enqueueMapBuffer");

  // Map cBuffer to host pointer. This enforces a sync with
  // the host backing space, remember we choose GPU device.
  const map = cl.enqueueMapBuffer(queue,
      cBuffer,      // cl buffer
      cl.TRUE,      // block
      cl.MAP_READ,  // flags
      0,            // offset
      size);        // size


  var output=":AFTER map: C = ";

  for (var i = 0; i < BUFFER_SIZE; i++) {
    output += C[i] + ", ";
  }

  logger.log(output);

  // we are now reading values as bytes, we need to cast it to the output type we want
  output = "output = ["+map.byteLength+" bytes] ";

  var map_view=new Uint8Array(map);

  for (var i = 0; i < map_view.length; i++) {
    output += map_view[i] + ", ";
  }

  logger.log(output);

  cl.enqueueUnmapMemObject(queue, cBuffer, map);

  output="after unmap C= ";

  for (var i = 0; i < BUFFER_SIZE; i++) {
    output += C[i] + ", ";
  }

  logger.log(output);

  cl.finish(queue); // Finish all the operations

  printResults(A,B,C);

  // cleanup
  // cl.releaseAll();
}

function printResults(A,B,C) {
  //Print input vectors and result vector
  var output = "\nA = ";
  for (var i = 0; i < BUFFER_SIZE; i++) {
    output += A[i] + ", ";
  }
  output += "\nB = ";
  for (var i = 0; i < BUFFER_SIZE; i++) {
    output += B[i] + ", ";
  }
  output += "\nC = ";
  for (var i = 0; i < BUFFER_SIZE; i++) {
    output += C[i] + ", ";
  }

  logger.log(output);
}

VectorAdd();

console.log("\n== Main thread terminated ==");
