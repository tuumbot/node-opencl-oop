#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const cv = require('opencv4nodejs');


const singleTest_pipe = () => {
  const mat0 = cv.imread(path.join(__dirname, '../assets/r3.png'));

  const {
    cl,
    OpenCLPipe,
  } = require('../src');

  const oclPipe = new OpenCLPipe();

  /* setup kernel parameter context */
  const inputSize = mat0.rows * mat0.cols * 3;

  /* uchar *inputBuffer */
  oclPipe.setup({
    kName: 'vImageInvert',
    source: fs.readFileSync(path.join(__dirname, './image-write.cl')).toString(),
    buffers: [
      {
        bName: 'inputBuffer',
        flags: cl.MEM_READ_WRITE | cl.MEM_USE_HOST_PTR,
        bufferSize: inputSize,
        bufferData: new Uint8Array(inputSize),
        bufferTypeStr: 'uchar*',
      },
      {
        bName: 'outputBuffer',
        flags: cl.MEM_WRITE_ONLY | cl.MEM_USE_HOST_PTR,
        bufferSize: inputSize,
        bufferData: new Uint8Array(inputSize),
        bufferTypeStr: 'uchar*',
      },
      {
        bName: 'iNumElements',
        flags: null,
        bufferSize: null,
        bufferData: inputSize / 3,
        bufferTypeStr: 'uint',
      },
    ],
  }, {
    width: mat0.cols,
    height: mat0.rows,
    channels: 3,
  });

  oclPipe.bufferPointer('inputBuffer', new Uint8Array(mat0.getData()))

  const outputBuffer = oclPipe.write('vImageInvert', 'outputBuffer');

  const mat1 = new cv.Mat(Buffer.from(outputBuffer), mat0.rows, mat0.cols, cv.CV_8UC3);

  oclPipe.finish();
  oclPipe.cleanup();

  cv.imshow('outputBuffer', mat1);
  cv.waitKey();
};

const streamTest_pipe = (mat0) => {
  // const mat0 = cv.imread(path.join(__dirname, '../assets/r3.png'));

  const {
    cl,
    OpenCLPipe,
  } = require('../src');

  const oclPipe = new OpenCLPipe();

  /* setup kernel parameter context */
  const inputSize = mat0.rows * mat0.cols * 3;

  /* uchar *inputBuffer */
  oclPipe.setup({
    kName: 'vImageInvert',
    source: fs.readFileSync(path.join(__dirname, './image-write.cl')).toString(),
    buffers: [
      {
        bName: 'inputBuffer',
        flags: cl.MEM_READ_WRITE | cl.MEM_USE_HOST_PTR,
        bufferSize: inputSize,
        bufferData: new Uint8Array(inputSize),
        bufferTypeStr: 'uchar*',
      },
      {
        bName: 'outputBuffer',
        flags: cl.MEM_WRITE_ONLY | cl.MEM_USE_HOST_PTR,
        bufferSize: inputSize,
        bufferData: new Uint8Array(inputSize),
        bufferTypeStr: 'uchar*',
      },
      {
        bName: 'iNumElements',
        flags: null,
        bufferSize: null,
        bufferData: inputSize / 3,
        bufferTypeStr: 'uint',
      },
    ],
  }, {
    width: mat0.cols,
    height: mat0.rows,
    channels: 3,
  });

  return oclPipe;
};

const asyncVideoStream = (callback) => {
  const devicePort = 0;
  const wCap = new cv.VideoCapture(devicePort);

  const delay = 10;
  let done = false;

  while (!done)
  {
    let frame = wCap.read();

    // loop back to start on end of stream reached
    if (frame.empty) {
      wCap.reset();

      frame = wCap.read();
    }

    let t0, t1;

    t0 = new Date();
    let mat = callback(frame);
    t1 = new Date();

    console.log({timeval: t1, deltaTime_ms: (t1 - t0) });

    cv.imshow('stream-test', mat);

    const key = cv.waitKey(delay);

    done = key == 27;
  }
};

if(require.main === module)
{
  let oclPipe = null;

  asyncVideoStream((frame) => {

    if(oclPipe == null)
    {
      /* initialize memory & context */
      oclPipe = streamTest_pipe(frame);
    }

    oclPipe.bufferPointer('inputBuffer', new Uint8Array(frame.getData()))

    const outputBuffer = oclPipe.write('vImageInvert', 'outputBuffer');

    const mat1 = new cv.Mat(Buffer.from(outputBuffer), frame.rows, frame.cols, cv.CV_8UC3);

    return mat1;
  });

  oclPipe.finish();
  oclPipe.cleanup();
  // streamTest();
}
