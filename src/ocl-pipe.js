
const cv = require('opencv4nodejs');

const { OpenCL } = require('./ocl-oop');

class OpenCLPipe {
  constructor() {
    this.ocl = null;

    this.inputs = [];
    this.outputs = [];
  }

  setup(oclParams, pipeParams) {
    if(!this.ocl) this.setupContext();

    const {
      width,
      height,
      channels,
    } = pipeParams;

    this.width = width;
    this.height = height;
    this.channels = channels;

    this.size = width * height * channels;

    this.iNumElements = width * height;

    this.ocl.createKernel(oclParams);
  }

  bufferPointer(bufferId, value) {
    if(!this.ocl.bufferMap[bufferId])
      throw new Error('Invalid buffer.');

    const buffer = this.ocl.bufferMap[bufferId];

    const bufferParams = buffer.params;

    bufferParams.bufferData = value;

    this.ocl.createBuffer(bufferParams, {
      bufferIndex: buffer.index,
      kernelBinding: buffer.kernel,
    });

    // this.ocl.bufferMap[bufferId].pointer = value;
  }

  setupContext() {
    const ocl = new OpenCL();

    this.ocl = ocl;

    return ocl;
  }

  write(kName, outputBufferId) {
    // if(!inputs.length == this.inputs.length)
    //  throw new Error('Invalid number of inputs.');

    /* sync input data */
    /*
    inputs.forEach((inputBuffer, bufferIndex) => {

      const {
        bufferId,
        bufferValue,
      } = inputBuffer;

      this.ocl.bufferMap[bufferId].pointer = bufferValue;
    });*/

    var localWS = null;
    var globalWS = [ this.iNumElements ];

    /* run kernel pass */
    this.ocl.enqueue(kName, {
      localWS,
      globalWS
    });

    const outputBuffer = this.ocl.syncBuffer(outputBufferId);

    return outputBuffer;
  }

  finish() {
    this.ocl.finish();
  }

  cleanup() {
    this.ocl.cleanup();
  }
};

module.exports = {
  OpenCLPipe,
};
