/** @file ocl-oop.js
 *
 *
 */
'use strict';

const assert = require('assert');

const cl = require("node-opencl");

const {
  contextProvider,
  programFactory,
  kernelFactory,
  bufferFactory,
  commandQueueProvider,
} = require('./ocl-factory');

class OpenCL {
  constructor(params) {
    this.bufferMap = {};
    this.kernelMap = {};

    this.device = null;
    this.context = null;
    this.queue = null;

    this.logger = params && params.logger ? params.logger : {
      log: console.log,
      error: console.error,
    };
  }

  setupContext(params) {
    const [device, context] = contextProvider();

    this.device = device;
    this.context = context;
  }

  setupQueue(params) {
    const {
      device,
      context
    } = this;

    const queue = commandQueueProvider({ context, device });

    if(!queue)
    {
      throw new Error(JSON.stringify({queue}));
    }

    this.queue = queue;
  }

  cleanup() {
    //  TODO: Clear memory mapping, buffers & context.
    cl.releaseAll();
  }

  createBuffer(bufferParams, kernelParams) {
    const {
      context,
    } = this;

    const {
      bName,
      flags,
      bufferSize,
      bufferTypeStr,
      bufferData,
    } = bufferParams;

    const {
      bufferIndex,
      kernelBinding,
    } = kernelParams;

    switch(bufferTypeStr) {
      case 'uint*':
      case 'uchar*':
        /* create buffer pointer bindings */
        const bufferBinding = bufferFactory({
          context,
          flags,
          bufferSize,
          bufferData,
        });

        this.bufferMap[bName] = {
          params: {
            bName, flags, bufferSize, bufferTypeStr,
          },
          index: bufferIndex,
          kernel: kernelBinding,
          binding: bufferBinding, //TODO: bufferFactory(...)
          map: null,
          pointer: bufferData,
        };

        cl.setKernelArg(kernelBinding, bufferIndex, bufferTypeStr, bufferBinding);
        break;
      case 'uint':

        this.bufferMap[bName] = {
          params: {

          },
          binding: bufferData,
          map: null,
          pointer: null,
        }

        /* set argument value */
        cl.setKernelArg(kernelBinding, bufferIndex, bufferTypeStr, bufferData);
        break;
    }
  }

  createKernel(params) {
    if(!this.context) this.setupContext();

    const {
      kName,
      source,
      buffers,
    } = params;

    const {
      context,
      device,
    } = this;

    /* compile kernel program */
    const program = programFactory({ device, context, source });

    let kernelBinding = kernelFactory({ device, program, kName });

    this.kernelMap[kName] = {
      params: {
        kName,
        bufferCount: buffers.length,
      },
      binding: kernelBinding, //TODO: kernelFactory(...)
    };

    buffers.forEach((bufferParams,bufferIndex) => {
      this.createBuffer(bufferParams, {bufferIndex,kernelBinding});
    });

  }

  enqueue(kName, params) {
    if(!this.kernelMap[kName] || !this.kernelMap[kName].binding)
      throw new Error(`:OpenCL: Kernel doesn't exist, kName = ${kName}.`);

    if(!this.queue) this.setupQueue();

    const {
      localWS,
      globalWS,
    } = params;

    const {
      queue
    } = this;

    const kernel = this.kernelMap[kName].binding;

    cl.enqueueNDRangeKernel(queue, kernel, 1,
        null,
        globalWS,
        localWS);
  }

  map(bufferId, params) {
    if(!this.bufferMap[bufferId] || !this.bufferMap[bufferId].binding)
      throw new Error(`:OpenCL: Buffer doesn't exist, bufferId = ${bufferId}.`);

    const {
      block,
      flags,
      offset,
    } = params;

    const {
      queue,
    } = this;

    const bufferBinding = this.bufferMap[bufferId].binding;
    const bufferSize = this.bufferMap[bufferId].params.bufferSize;

    assert(bufferSize != 0, new Error(`:OpenCL.map: Invalid buffer, bufferId = ${bufferId}, bufferSize = ${bufferSize}.`))

    // Map cBuffer to host pointer. This enforces a sync with
    // the host backing space, remember we choose GPU device.

    const map = cl.enqueueMapBuffer(queue,
        bufferBinding,
        block,
        flags,
        offset,
        bufferSize);

    this.bufferMap[bufferId].map = map;

    return map;
  }

  unmap(bufferId) {
    if(!this.bufferMap[bufferId] || !this.bufferMap[bufferId].map)
      throw new Error(`:OpenCL.unmap: Invalid buffer, bufferId = ${bufferId}.`);

    const {
      queue
    } = this;

    const {
      binding,
      map,
    } = this.bufferMap[bufferId];

    cl.enqueueUnmapMemObject(queue, binding, map);
  }

  syncBuffer(bufferId) {

    const map = this.map(bufferId, {
      block: cl.TRUE,
      flags: cl.MAP_READ,
      offset: 0,
    });

    this.unmap(bufferId);

    return this.bufferMap[bufferId].pointer;
  }

  finish() {
    cl.finish(this.queue);
  }

};

if(require.main === module)
{

}

module.exports = {
  OpenCL,
};
