#!/usr/bin/env node
/** @file ocl
 *
 *
 */
'use strict';

const cl = require("node-opencl");

/* OpenCL: Context interface */
const contextProvider = (params) => {
  /* create GPU context for this platform */
  const context = cl.createContextFromType(
    [
      cl.CONTEXT_PLATFORM,
      cl.getPlatformIDs()[0],
    ],
    cl.DEVICE_TYPE_GPU,
    null, null);

  var devices=cl.getContextInfo(context, cl.CONTEXT_DEVICES);
  var device=devices[0];

  return [device, context];
};

/* OpenCL: Program interface */
const programFactory = (params) => {
  const {
    device,
    context,
    source,
  } = params;

  const program = cl.createProgramWithSource(context, source);

  try {
    cl.buildProgram(program);
  }
  catch(err) {
    console.error(':ocl.programFactory:', err, cl.getProgramBuildInfo(program, device, cl.PROGRAM_BUILD_LOG));
  }

  return program;
};


const bufferFactory = (params) => {
  const {
    context,
    flags,
    bufferSize,
    bufferData,
  } = params;

  return cl.createBuffer(context, flags, bufferSize, bufferData);
};

const kernelFactory = (params) => {
  const {
    device,
    program,
    kName,
  } = params;

  //Create kernel object
  let kernel = null;

  try {
    kernel = cl.createKernel(program, kName);
  }
  catch(err) {
    console.log(':ocl.kernelFactory:', {kName});
    console.error(':ocl.kernelFactory:', err, cl.getProgramBuildInfo(program, device, cl.PROGRAM_BUILD_LOG));
  }

  return kernel;
};

const commandQueueProvider = (params) => {
  const {
    context,
    device,
  } = params;

  let queue = null;

  if (cl.createCommandQueueWithProperties !== undefined) {
    queue = cl.createCommandQueueWithProperties(context, device, []); // OpenCL 2
  } else {
    queue = cl.createCommandQueue(context, device, null); // OpenCL 1.x
  }

  return queue;
};

module.exports = {
  contextProvider,

  programFactory,
  bufferFactory,
  kernelFactory,

  commandQueueProvider,
};
