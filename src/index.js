#!/usr/bin/env node
/** @file ocl
 *
 *
 */
'use strict';

const cl = require("node-opencl");

const {
  contextProvider,

  programFactory,
  bufferFactory,
  kernelFactory,

  commandQueueProvider,
} = require('./ocl-factory');

const {
  OpenCL
} = require('./ocl-oop');

const {
  OpenCLPipe,
} = require('./ocl-pipe');

module.exports = {
  contextProvider,

  programFactory,
  bufferFactory,
  kernelFactory,

  commandQueueProvider,

  cl,
  OpenCL,
  OpenCLPipe,
};
