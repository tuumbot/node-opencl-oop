#!/usr/bin/env node
/** @file ocl
 *
 *
 */
'use strict';

const {
	cl,

  contextProvider,

  programFactory,
  bufferFactory,
  kernelFactory,

  commandQueueProvider,

  OpenCL,
  OpenCLPipe,
} = require('./src');

if(require.main === module)
{
	console.log({OpenCL,OpenCLPipe});
}

module.exports = {
  contextProvider,

  programFactory,
  bufferFactory,
  kernelFactory,

  commandQueueProvider,

  OpenCL,
  OpenCLPipe,

  cl,
};
